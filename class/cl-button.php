<?php
// defino clase
class Button{
// caracteristicas del mouse
public $botonPos;
public $botonForma;
public $botonColor;
public $botonAlto;
public $botonAncho;

// constructor
public function __construct($posBtn, $clr, $frm, $alt, $anc)
    {
        $this->ButtonPos = $posBtn;
        $this->ButtonColor = $clr;
        $this->ButtonForma = $frm;
        $this->ButtonAlto = $alt;
        $this->ButtonAncho = $anc;
    }

    public function setOnClick($ev){
        if($ev){
            $this->clickNow = 'Hiciste click';
        }else{
            $this->clickNow = 'Hacer click';
        }
    }

    public function getOnClick(){
        return $this->clickNow;
    }

}
?>