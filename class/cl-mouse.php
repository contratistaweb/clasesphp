<?php

class Mouse
{
    public $boton;
    public $forma;
    public $color;
    public $tConn;
    public $tConnNow;
    public $marca;
    public $tipoBat;
    public $alto;
    public $ancho;

    // constructor
    public function __construct($btn, $frm, $clr, $conn, $now, $mrc, $tBat, $alt, $anc, $st)
    {
        $this->Botones = $btn;
        $this->Forma = $frm;
        $this->Color = $clr;
        $this->Conexion = $conn;
        $this->ConectNow = $now;
        $this->Marca = $mrc;
        $this->TipoBat = $tBat;
        $this->Alto = $alt;
        $this->Ancho = $anc;
        $this->State = $st;
    }

    // conectado usb o wireless 
    public function connected($connected)
    {
        $con = !$connected;
        return $con;
    }

    // estado encendido o apagado
    public function setOnOff($st)
    {
        if($st){
            $this->ConectNow = 'disponible';
        }else{
            $this->ConectNow = 'no disponible';
        }
    }

    // comprueba estado
    public function getOnOff()
    {
        return $this->ConectNow;
    }

    // 
    public function getState()
    {
        return $this->State;
    }

}
?>