<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Clases PHP</title>
</head>
<!-- repositorio: https://bitbucket.org/contratistaweb/clasesphp/src/master/ -->
<body>
    <?php
    require('./class/cl-mouse.php');
    require('./class/cl-button.php');

    $mouse1 = new Mouse(3, 'ergonomico', 'negro', 'inalambrico', false, 'Genius', 'litio', '100 mm', '100 mm', ' (Disponible)');
    $mouse1buttonIzq = new Button('izquierdo', 'negro', 'rectangular', '3 cm', '2 cm');
    $mouse1buttonCenter = new Button('centro', 'azul', 'rueda', '1 cm', '1.5 cm');
    $mouse1buttonDer = new Button('derecho', 'negro', 'rectangular', '3 cm', '2 cm');

    $mouse2 = new Mouse(3, 'ergonomico', 'gris', 'alambrico', false, 'Acer', 'AAA', '120 mm', '100 mm', ' (Agotado)');
    $mouse2buttonIzq = new Button('izquierdo', 'negro', 'rectangular', '2.5 cm', '3 cm');
    $mouse2buttonCenter = new Button('centro', 'azul', 'rueda', '1 cm', '1.5 cm');
    $mouse2buttonDer = new Button('derecho', 'negro', 'rectangular', '2.5 cm', '3 cm');

    ?>

    <div class="container">
        <div class="row my-5">
            <div class="col-12 my-2">
                <h1>Mouse <?php echo $mouse1->Marca; echo $mouse1->getState(); ?></h1>
            </div>
            <div class="col-12 row justify-content-start card shadow mx-auto">
                <div class="col-12">
                    <h3 class="text-center">Caracteristicas</h3>
                </div>

                <div class="col-6">
                    <p>Botones: <?php echo $mouse1->Botones; ?></p>
                </div>

                <div class="col-6">
                    <p>Forma: <?php echo $mouse1->Forma; ?></p>
                </div>

                <div class="col-6">
                    <p>Color: <?php echo $mouse1->Color; ?></p>
                </div>

                <div class="col-6">
                    <p>Marca: <?php echo $mouse1->Marca; ?></p>
                </div>

                <div class="col-6">
                    <p>Conexion: <?php echo $mouse1->Conexion; $mouse1->setOnOff(true); echo " ".$mouse1->getOnOff();?></p>
                </div>

                <div class="col-6">
                    <p>Tipo de Bateria: <?php echo $mouse1->TipoBat; ?></p>
                </div>

                <div class="col-6">
                    <p>Alto: <?php echo $mouse1->Alto; ?></p>
                </div>

                <div class="col-6">
                    <p>Ancho: <?php echo $mouse1->Ancho; ?></p>
                </div>

            </div>

            <div class="col-4 card shadow mx-auto">

                <div class="col-12">
                    <h3 class="text-center">Boton <?php echo $mouse1buttonIzq->ButtonPos; ?></h3>
                </div>

                <div class="col-6">
                    <p>Color: <?php echo $mouse1buttonIzq->ButtonColor; ?></p>
                </div>

                <div class="col-6">
                    <p>Forma: <?php echo $mouse1buttonIzq->ButtonForma; ?></p>
                </div>

                <div class="col-6">
                    <p>Alto: <?php echo $mouse1buttonIzq->ButtonAlto; ?></p>
                </div>

                <div class="col-6">
                    <p>Ancho: <?php echo $mouse1buttonIzq->ButtonAncho; ?></p>
                </div>

            </div>
            <div class="col-4 card shadow mx-auto">

                <div class="col-12">
                    <h3 class="text-center">Boton <?php echo $mouse1buttonCenter->ButtonPos; ?></h3>
                </div>

                <div class="col-6">
                    <p>Color: <?php echo $mouse1buttonCenter->ButtonColor; ?></p>
                </div>

                <div class="col-6">
                    <p>Forma: <?php echo $mouse1buttonCenter->ButtonForma; ?></p>
                </div>

                <div class="col-6">
                    <p>Alto: <?php echo $mouse1buttonCenter->ButtonAlto; ?></p>
                </div>

                <div class="col-6">
                    <p>Ancho: <?php echo $mouse1buttonCenter->ButtonAncho; ?></p>
                </div>

            </div>
            <div class="col-4 card shadow mx-auto">
                <div class="col-12">
                    <h3 class="text-center">Boton <?php echo $mouse1buttonDer->ButtonPos; ?></h3>
                </div>

                <div class="col-6">
                    <p>Color: <?php echo $mouse1buttonDer->ButtonColor; ?></p>
                </div>

                <div class="col-6">
                    <p>Forma: <?php echo $mouse1buttonDer->ButtonForma; ?></p>
                </div>

                <div class="col-6">
                    <p>Alto: <?php echo $mouse1buttonDer->ButtonAlto; ?></p>
                </div>

                <div class="col-6">
                    <p>Ancho: <?php echo $mouse1buttonDer->ButtonAncho; ?></p>
                </div>

            </div>
        </div>

        <div class="row my-5">
            <div class="col-12 my-2">
                <h1>Mouse <?php echo $mouse2->Marca; echo $mouse2->getState();?></h1>
            </div>
            <div class="col-12 row justify-content-start card shadow mx-auto">
                <div class="col-12">
                    <h3 class="text-center">Caracteristicas</h3>
                </div>
                <div class="col-6">
                    <p>Botones: <?php echo $mouse2->Botones; ?></p>
                </div>

                <div class="col-6">
                    <p>Forma: <?php echo $mouse2->Forma; ?></p>
                </div>

                <div class="col-6">
                    <p>Color: <?php echo $mouse2->Color; ?></p>
                </div>

                <div class="col-6">
                    <p>Marca: <?php echo $mouse2->Marca; ?></p>
                </div>

                <div class="col-6">
                    <p>Conexion: <?php echo $mouse2->Conexion;  $mouse2->setOnOff(false); echo " ".$mouse2->getOnOff(); ?></p>
                </div>

                <div class="col-6">
                    <p>Tipo de Bateria: <?php echo $mouse2->TipoBat; ?></p>
                </div>

                <div class="col-6">
                    <p>Alto: <?php echo $mouse2->Alto; ?></p>
                </div>

                <div class="col-6">
                    <p>Ancho: <?php echo $mouse2->Ancho; ?></p>
                </div>

            </div>
            <div class="col-4 card shadow mx-auto">
                <div class="col-12">
                    <h3 class="text-center">Boton <?php echo $mouse2buttonIzq->ButtonPos; ?></h3>
                </div>

                <div class="col-6">
                    <p>Color: <?php echo $mouse2buttonIzq->ButtonColor; ?></p>
                </div>

                <div class="col-6">
                    <p>Forma: <?php echo $mouse2buttonIzq->ButtonForma; ?></p>
                </div>

                <div class="col-6">
                    <p>Alto: <?php echo $mouse2buttonIzq->ButtonAlto; ?></p>
                </div>

                <div class="col-6">
                    <p>Ancho: <?php echo $mouse2buttonIzq->ButtonAncho; ?></p>
                </div>

            </div>
            <div class="col-4 card shadow mx-auto">
                <div class="col-12">
                    <h3 class="text-center">Boton <?php echo $mouse2buttonCenter->ButtonPos; ?></h3>
                </div>

                <div class="col-6">
                    <p>Color: <?php echo $mouse2buttonCenter->ButtonColor; ?></p>
                </div>

                <div class="col-6">
                    <p>Forma: <?php echo $mouse2buttonCenter->ButtonForma; ?></p>
                </div>

                <div class="col-6">
                    <p>Alto: <?php echo $mouse2buttonCenter->ButtonAlto; ?></p>
                </div>

                <div class="col-6">
                    <p>Ancho: <?php echo $mouse2buttonCenter->ButtonAncho; ?></p>
                </div>

            </div>
            <div class="col-4 card shadow mx-auto">
                <div class="col-12">
                    <h3 class="text-center">Boton <?php echo $mouse2buttonDer->ButtonPos; ?></h3>
                </div>

                <div class="col-6">
                    <p>Color: <?php echo $mouse2buttonDer->ButtonColor; ?></p>
                </div>

                <div class="col-6">
                    <p>Forma: <?php echo $mouse2buttonDer->ButtonForma; ?></p>
                </div>

                <div class="col-6">
                    <p>Alto: <?php echo $mouse2buttonDer->ButtonAlto; ?></p>
                </div>

                <div class="col-6">
                    <p>Ancho: <?php echo $mouse2buttonDer->ButtonAncho; ?></p>
                </div>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>

</html>